package com.test;

import com.test.ArrayList.MyIndexOutOfBoundsException;

/**
 * @author zhb
 * @date 2021/8/20 22:08
 * @desc
 */
public interface List {
    public int size();

    public Object get(int i) throws MyIndexOutOfBoundsException;

    public boolean isEmpty();

    public boolean contains(Object o);

    public int indexOf(Object o);

    public void add(int i, Object o);

    public void add(Object o);

    public boolean addBefore(Object obj, Object o);

    public boolean addAfter(Object obj, Object o);

    public Object remove(int i);

    public boolean remove(Object o);

    public Object replace(int i,Object o);
}
