package com.test.LinkedList;

/**
 * @author zhb
 * @date 2021/8/21 11:00
 * @desc
 */
public class Node {
    Object data;//数据域
    Node next;//指针 指向下一个节点

    public Node() {
    }

    public Node(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
