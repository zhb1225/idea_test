package com.test.LinkedList;

import com.test.ArrayList.ArrayList;
import com.test.List;

/**
 * @author zhb
 * @date 2021/8/20 22:14
 * @desc
 */
public class TestLinkedList {
    public static void main(String[] args) {
        List list = new LinkedList();
        list.add(123);
        list.add(321);
        list.add(456);
        list.add(789);
        list.add(333);
        System.out.println(list.size());
        System.out.println(list.isEmpty());
        System.out.println(list.get(4));
        System.out.println(list.contains(111));
        System.out.println(list.indexOf(333));
        System.out.println(list);
        list.add(1, 12345);
        list.add(0,666);
        System.out.println(list.indexOf(12345));
        System.out.println(list);
        list.remove(2);
        System.out.println(list);
    }
}
