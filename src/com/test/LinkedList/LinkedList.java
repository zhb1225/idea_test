package com.test.LinkedList;

import com.test.ArrayList.MyIndexOutOfBoundsException;
import com.test.List;

/**
 * @author zhb
 * @date 2021/8/21 10:57
 * @desc
 */
public class LinkedList implements List {
    private Node head = new Node();//头结点 不存储数据 为了编程方便
    private int size;


    @Override
    public int size() {
        return size;
    }

    @Override
    public Object get(int i) throws MyIndexOutOfBoundsException {
//        和顺序表不一样了
        Node node = new Node();
        node = head;
        for (int j = 0; j < size; j++) {
            node = node.next;
            if(j==i){
               break;
            }
        }
        return node.data;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        if(indexOf(o)!=-1){
            return true;
        }
        return false;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        Node node = head;
        for (int i = 0; i < size; i++) {
            node=node.next;
            if(node.data.equals(o)){
                index = i;
            }

        }
        return index;
    }

    @Override
    public void add(int i, Object o) {
        Node node = new Node(o);
        Node d = head;
        for (int j = 0; j < i; j++) {
            d=d.next;
        }
        node.next=d.next;
        d.next=node;
        size++;
    }

    @Override
    public void add(Object o) {
        this.add(size,o);
    }

    @Override
    public boolean addBefore(Object obj, Object o) {
        int index = indexOf(obj);
        if(index==-1){
            return false;
        }
        add(index,o);
        return true;
    }

    @Override
    public boolean addAfter(Object obj, Object o) {
        int index = indexOf(obj);
        if(index==-1){
            return false;
        }
        add(index+1,o);
        return true;
    }

    @Override
    public Object remove(int i) {
        Object o = get(i);
        Node node = head;
        for (int j = 0; j < i; j++) {
            node=node.next;
        }
        node.next=node.next.next;
        size--;
        return null;
    }

    @Override
    public boolean remove(Object o) {
        int index= indexOf(o);
        remove(index);
        return false;
    }

    @Override
    public Object replace(int i, Object o) {

        return null;
    }

    @Override
    public String toString() {
        if(size==0){
            return "[]";
        }else{
            StringBuilder sb = new StringBuilder("[");
            Node p = new Node();
            p = head;
            for (int i = 0; i < size; i++) {
                sb.append(p.next.data+" ");
                p=p.next;
            }
            sb.append("]");
            return sb.toString();
        }
    }
}
