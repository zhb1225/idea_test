package com.test.DiyTree;

/**
 * @author zhb
 * @date 2021/8/21 18:30
 * @desc
 */
public interface BinaryTree {
    public boolean isEmpty();

    public int size();//节点数量

    public int getHeight();//获取二叉树的高度

    public Node findKey(int value);//查询指定的节点

    public void preOrderTraverse();//先序遍历

    public void inOrderTraverse();//中序遍历

    public void postOrderTraverse();//后序遍历

    public void preOrderTraverse(Node node);//先序遍历

    public void inOrderTraverse(Node node);//中序遍历

    public void postOrderTraverse(Node node);//后序遍历

    public void levelOrderByStack();//按照层次遍历

    public void inOrderByStack();

    public void preOrderByStack();
}
