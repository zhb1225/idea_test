package com.test.DiyTree;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author zhb
 * @date 2021/8/21 18:34
 * @desc
 */
public class LinkedBinaryTree implements BinaryTree {
    private Node root;//根节点

    public LinkedBinaryTree(Node root) {
        this.root = root;
    }

    public LinkedBinaryTree() {
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int size() {
        System.out.print("二叉树节点的个数:");
        return this.size(root);
    }

    public int size(Node node) {
        if (node != null) {
            int nleft = this.size(node.leftChild);
            int nright = this.size(node.rightChild);
            return nleft + nright + 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getHeight() {
        return this.getHeight(root);
    }

    public int getHeight(Node node) {
        if (node != null) {
            int nleft = this.getHeight(node.leftChild);
            int nright = this.getHeight(node.rightChild);
            return nleft > nright ? nleft + 1 : nright + 1;
        } else {
            return 0;
        }
    }

    @Override
    public Node findKey(int value) {
        return findKey(value,root);
    }
    public Node findKey(Object value,Node node) {
        if (node == null) {
            return null;
        } else if(node.value.equals(value)){
           return node;
        }else{
            Node node1 = this.findKey(value,node.leftChild);
            Node node2 = this.findKey(value,node.rightChild);
            if(node1!=null&&node1.value.equals(value)){
                return node1;
            }else if(node2!=null&&node2.value.equals(value)){
                return node2;
            }else{
                return null;
            }
        }
    }

    @Override
    public void preOrderTraverse() {
//        输出根节点的值
        if (root != null) {
            System.out.print(root.value + " ");
            //        对左子树进行先序遍历
            BinaryTree leftTree = new LinkedBinaryTree(root.leftChild);
            leftTree.preOrderTraverse();
            //        对右子树进行先序遍历
            BinaryTree rightTree = new LinkedBinaryTree(root.rightChild);
            rightTree.preOrderTraverse();
        }

    }

    @Override
    public void preOrderTraverse(Node node) {

    }

    @Override
    public void inOrderTraverse() {
        System.out.print("中序遍历:");
        inOrderTraverse(root);
        System.out.println();
    }

    @Override
    public void inOrderTraverse(Node node) {
        if (node != null) {

            //        对左子树进行先序遍历
            this.inOrderTraverse(node.leftChild);
//            输出根节点
            System.out.print(node.value + " ");
            //        对右子树进行先序遍历
            this.inOrderTraverse(node.rightChild);
        }
    }

    @Override
    public void postOrderTraverse() {
        System.out.print("后序遍历:");
        postOrderTraverse(root);
        System.out.println();
    }

    @Override
    public void postOrderTraverse(Node node) {
        if (node != null) {

            //        对左子树进行先序遍历
            this.postOrderTraverse(node.leftChild);
            //        对右子树进行先序遍历
            this.postOrderTraverse(node.rightChild);
            //            输出根节点
            System.out.print(node.value + " ");
        }
    }

    @Override
    public void levelOrderByStack() {
        System.out.print("按照层次遍历二叉树：");
        if(root==null){
            return;
        }
//        队列
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while(queue.size()!=0){
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                Node temp = queue.poll();
                System.out.print(temp.value+" ");
                if(temp.leftChild!=null){
                    queue.add(temp.leftChild);
                }
                if(temp.rightChild!=null){
                    queue.add(temp.rightChild);
                }
            }
        }
        System.out.println();
    }
//    中序非递归遍历
    @Override
    public void inOrderByStack(){
//        栈
        System.out.print("中序非递归遍历:");
        Deque<Node> stack = new LinkedList<>();
        Node result = root;
        while(result!=null||!stack.isEmpty()){
            while(result!=null){
                stack.push(result);
                result=result.leftChild;
            }
            if(!stack.isEmpty()){
                result=stack.pop();
                System.out.print(result.value+" ");
                result=result.rightChild;
            }
        }
        System.out.println();
    }

    @Override
    public void preOrderByStack() {
        System.out.print("前序非递归遍历:");
        Deque<Node> stack = new LinkedList<>();
        Node result = root;
        while(result!=null||!stack.isEmpty()){
            while(result!=null){
                stack.push(result);
                System.out.print(result.value+" ");
                result=result.leftChild;
            }
            if(!stack.isEmpty()){
                result=stack.pop();
                result=result.rightChild;
            }
        }
    }
}
