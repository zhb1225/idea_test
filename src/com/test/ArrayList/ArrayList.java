package com.test.ArrayList;

import com.test.List;

import java.util.Arrays;

/**
 * @author zhb
 * @date 2021/8/20 22:12
 * @desc
 */
public class ArrayList implements List {
    //    底层是一个数组 还没有定义长度
    transient Object[] elementData;
    //不是数组分配了几个空间 而是元素的个数
    private int size;

    public ArrayList() {
//        没有指定长度 默认长度是4
        this(4);
//        没有指定长度 长度是0
//        elementData = new Object[]{};
    }

    public ArrayList(int initialCapacity) {
        //    给数组分配指定数量的空间
        elementData = new Object[initialCapacity];
        //    指定顺序表的元素个数 默认是0
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object get(int i) {
        if (i < size&&i>=0) {
            return elementData[i];
        } else {
            throw new MyIndexOutOfBoundsException(i);
        }
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (elementData[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (elementData[i].equals(o)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void add(int i, Object o) {
        if(i<0||i>size){
            throw new MyIndexOutOfBoundsException(i);
        }
        if(size == elementData.length){
            grow();
        }
        for (int j = size; j > i ; j--) {
            elementData[j]=elementData[j-1];
        }
        elementData[i] = o;
        size++;
    }

    @Override
    public String toString() {
        if(size==0){
            return "[]";
        }
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            if(i!=size-1){
                sb.append(elementData[i]+",");
            }else {
                sb.append(elementData[i]);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public void add(Object o) {
        this.add(size,o);
    }

    private void grow(){
        elementData=Arrays.copyOf(elementData,elementData.length*2);
    }

    @Override
    public boolean addBefore(Object obj, Object o) {
        int index= this.indexOf(obj);
        if(index!=-1){
            add(index,o);
            return true;
        }
        return false;
    }

    @Override
    public boolean addAfter(Object obj, Object o) {
        int index= this.indexOf(obj);
        if(index!=-1){
            add(index+1,o);
            return true;
        }
        return false;
    }

    @Override
    public Object remove(int i) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public Object replace(int i, Object o) {
        return null;
    }
}
