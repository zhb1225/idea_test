package com.test.ArrayList;

/**
 * @author zhb
 * @date 2021/8/20 22:41
 * @desc
 */
//自定义异常
public class MyIndexOutOfBoundsException extends RuntimeException{
    public MyIndexOutOfBoundsException() {
        super();
    }

    public MyIndexOutOfBoundsException(int i) {
        super("数组越界异常"+i);
    }
}
