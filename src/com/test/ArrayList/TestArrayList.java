package com.test.ArrayList;

/**
 * @author zhb
 * @date 2021/8/20 22:14
 * @desc
 */
public class TestArrayList {
    public static void main(String[] args)  {
        ArrayList list = new ArrayList();
        list.add(123);
        list.add(321);
        list.add(456);
        list.add(789);
        list.add(333);
        System.out.println(list.size());
        System.out.println(list.isEmpty());
        System.out.println(list.get(4));
        System.out.println(list.contains(111));
        System.out.println(list.indexOf(111));
        System.out.println(list);
        list.add(1,12345);
        System.out.println(list.indexOf(12345));
        System.out.println(list);
    }
}
